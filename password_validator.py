"""
Write a function that checks if a given password is valid. Password validations are:
The length should be 6 - 10 characters (inclusive)
It should consists only letters and digits
It should have at least 2 digits 

If a password is valid print "Password is valid".
If it is NOT valid, for every unfulfilled rule print a message:

"Password must be between 6 and 10 characters"
"Password must consist only of letters and digits"
"Password must have at least 2 digits"

Example:

Input: logIn

Output:

Password must be between 6 and 10 characters
Password must have at least 2 digits

"""

def password_validator(password):
    result_char = []
    result_num = 0
    special_char = 0
    for ch in password:
        if ch.isdigit():
            result_num += 1   
        elif ch.isalpha():
            result_char.append(ch)
        else:
            special_char += 1
    
    if not 6 <= len(password) <= 10:
        print("Password must be between 6 and 10 characters.")
    
    if result_num < 2:
        print("Password must have least 2 digitis.")
    
    if special_char:
        print("Password must consist only of letters and digits")
        
    if len(result_char) >= 1 and result_num >= 2 and 6 <= len(password) <= 10:
        print("Password is valid")

password_validator("MyPass123")
password_validator("logIn")
password_validator("Pa$s$s")

