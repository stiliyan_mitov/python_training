"""
Problem:
You have been given a list of words. Your task is to write a Python program that finds the word with the highest score. The score of a word is calculated by summing the ASCII values of its characters.

Instructions:

Define a function called calculate_score that takes a word as an argument and returns the score of the word.
In the calculate_score function, iterate over each character in the word and sum the ASCII values of all the characters.
Define another function called find_highest_score_word that takes a list of words as an argument and returns the word with the highest score.
In the find_highest_score_word function, iterate over each word in the list and use the calculate_score function to calculate the score of each word.
Keep track of the highest score and the word associated with it.
Finally, return the word with the highest score.

Note:

You can assume that the input list will not be empty.
In case of ties, return the first word with the highest score.

Example:

words = ["hello", "world", "python", "programming"]
print(find_highest_score_word(words))

Output:

programming

"""

def calculate_score(word):
    score = 0
    for ch in word:
        score += ord(ch)
    
    return score
        

def find_highest_score_word(list_of_words):
    highest_score = calculate_score(list_of_words[0])
    print(f"This is the higest_score in the moment: {highest_score}")
    
    for word in list_of_words:
        if calculate_score(word) > highest_score:
            highest_score = calculate_score(word)
            
            word_with_hight_score = word
            
    
    return word_with_hight_score
            

words = ["hello", "world", "python", "programming"]
              
print(find_highest_score_word(words))