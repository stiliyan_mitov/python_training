"""
Here comes the final and the most interesting part – the Final ranking of the candidate-interns. The final ranking is determined by the points of the interview tasks and from the exams in SoftUni. Here is your final task. You will receive some lines of input in the format "{contest}:{password for contest}" until you receive "end of contests". Save that data because you will need it later. After that you will receive other type of inputs in format "{contest}=>{password}=>{username}=>{points}" until you receive "end of submissions". Here is what you need to do.
 Check if the contest is valid (if you received it in the first type of input)
 Check if the password is correct for the given contest
 Save the user with the contest they take part in (a user can take part in many contests) and the points the user has in the given contest. If you receive the same contest and the same user update the points only if the new ones are more than the older ones.
At the end you have to print the info for the user with the most points in the format "Best candidate is {user} with total {total points} points.". After that print all students ordered by their names. For each user print each contest with the points in descending order. See the examples.

Input:
 strings in format "{contest}:{password for contest}" until the "end of contests" command. There will be no case with two equal contests
 strings in format "{contest}=>{password}=>{username}=>{points}" until the "end of submissions" command.
 There will be no case with 2 or more users with same total points!

Output
 On the first line print the best user in format "Best candidate is {user} with total {total points} points.".
 Then print all students ordered as mentioned above in format:
{user1 name}
#  {contest1} -> {points}
#  {contest2} -> {points}
"""

result = {}

while True:
    command = input()

    if command == 'end of contests':
        break

    splited_command = command.split(':')
    contest = splited_command[0]
    password = splited_command[1]

    if contest not in result:
        result[contest] = password


print(result)

result_user = {}

while True:

    command = input()

    if command == 'end of submissions':
        break

    splited_command = command.split('=>')
    contest = splited_command[0]
    password = splited_command[1]
    username = splited_command[2]
    points = splited_command[3]

    if contest in result and result[contest] == password:
        if username not in result_user:
            result_user[username] = [contest]
            result_user[username] += [points]
        else:
            result_user[username] += [contest]
            result_user[username] += [points]


print(result_user)

new_dict = {}

for key, value in result_user.items():
    new_dict[key]  = {str(i): item for i, item in enumerate(value)}

print(new_dict)


# for key in result_user.keys():
#     for val in result_user[key]:
#         print(val)
