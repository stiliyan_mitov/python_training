command = input()

stack = []

for i in command:
    stack.append(i)

result = []

while len(stack) > 0:
    result.append(stack.pop())

print(''.join(result))




