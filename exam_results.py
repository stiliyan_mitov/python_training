"""
Judge statistics on the last Programing Fundamentals exam was not working correctly, so you have the task to take all the submissions and analyze them properly. You should collect all the submissions and print the final results and statistics about each language that the participants submitted their solutions in.
You will be receiving lines in the following format: "{username}-{language}-{points}" until you receive "exam finished". You should store each username and his submissions and points.
You can receive a command to ban a user for cheating in the following format: "{username}-banned". In that case, you should remove the user from the contest, but preserve his submissions in the total count of submissions for each language.
After receiving "exam finished" print each of the participants, ordered descending by their max points, then by username, in the following format:
Results:
{username} | {points}
…
After that print each language, used in the exam, ordered descending by total submission count and then by language name, in the following format:
Submissions:
{language} – {submissionsCount}
"""

result = {}
submissions = {}

while True:
    command = input()

    if command == "exam finished":
        break

    split_command = command.split('-')
    name = split_command[0]
    lenguage = split_command[1]

    if name not in result:
        points = split_command[2]
        result[name] = points

    if split_command[1] == "banned" and name in result:
        del(result[name])
        continue

    if lenguage not in submissions:
        submissions[lenguage] = 1
    else:
        submissions[lenguage] += 1

print(f"This is the result: {result}")
print(f"This is the submissions: {submissions}")

sorted_result = dict(sorted(result.items(), key=lambda item: item[1], reverse=True))
sorted_submissions = dict(sorted(submissions.items(), key=lambda item: item[1], reverse=True))

print(f"This is the sorted result: {sorted_result}")
print(f"This is the sorted submissions: {sorted_submissions}")

print("Result:")
for key, value in sorted_result.items():
    print(f"{key} | {value}")

print("Submissions:")
for key, value in sorted_submissions.items():
    print(f"{key} | {value}")
