"""Problem:
You are given a list of numbers. Write a Python program that finds the two numbers in the list that have the smallest absolute difference between them.

Instructions:

Define a function called find_smallest_difference that takes a list of numbers as an argument and returns a tuple containing the two numbers with the smallest absolute difference.
In the find_smallest_difference function, iterate over each pair of numbers in the list and calculate the absolute difference between them.
Keep track of the smallest absolute difference encountered so far, as well as the two numbers associated with it.
Finally, return a tuple containing the two numbers with the smallest absolute difference.
Note:

You can assume that the input list will contain at least two numbers.
In case of ties, return the pair of numbers that appear first in the list.

Example:

numbers = [4, 9, 2, 6, 7, 1, 5]
print(find_smallest_difference(numbers))

Output:

(4, 5)

"""

def find_smallest_difference(numbers):
    result = None
    small_different = float("inf")
    
    for i in range(len(numbers)):
        for j in range(i + 1, len(numbers)):
            diff = abs(numbers[i] - numbers[j])
            if diff < small_different:
                small_different = diff
                result = (numbers[i], numbers[j])
            
    return result
             
numbers = [4, 9, 2, 6, 7, 1, 5]
print(find_smallest_difference(numbers))